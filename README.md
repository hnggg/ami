Advanced Mechanized Intelligence

A C library and script using such for an AMI, known more as Artifical Intelligence, it has a key part to it, and that is the brain, of which is dynamically allocated based on the AMI_BRAIN variable, located in the main header file. From this variable, it is divided into categorys based on their needs, for example word tones would be allocated a great amount because of the sheer need.

The AMI works by parsing the sentence down to the tones and indicator language used, then it can interperate the context behind it to not only analyse it, but the AMI's demo bot also uses it and displays an appropriate return, likewise the AMI can be used to analyse the means of the phrase and to detect the tone of voice used by it.

It is written in C with header files and several .source files, of which are just text files used by the system to parse its content. Returns are mostly some kind of structure, these are in the corrosponding header file
